# Теги для моделей

[![Latest Version on Packagist](https://img.shields.io/packagist/v/mihakot/laravel-tags.svg?style=flat-square)](https://packagist.org/packages/mihakot/laravel-tags)
[![Total Downloads](https://img.shields.io/packagist/dt/mihakot/laravel-tags.svg?style=flat-square)](https://packagist.org/packages/mihakot/laravel-tags)


## Installation

You can install the package via composer:

```bash
composer require mihakot/laravel-tags
```
### Publish provider config

```bash
artisan vendor:publish --provider=Mihakot\Tags\TagsServiceProvider
```

## Usage

Here are some code examples:

```php
// apply HasTags trait to a model
use Illuminate\Database\Eloquent\Model;
use Mihakot\Tags\HasTags;

class ArticleItem extends Model
{
    use HasTags;
    
    // ...
}
```
```php

// Create a model with tags
$articleItem = ArticleItem::create([
   'name' => 'The Article Title',
   'tags' => ['first tag', 'second tag'], //tags will be created if they don't exist
]);

// Attaching tags
$articleItem->attachTag('third tag');
$articleItem->attachTag('third tag','some_type');
$articleItem->attachTags(['fourth tag', 'fifth tag']);
$articleItem->attachTags(['fourth_tag','fifth_tag'],'some_type');

// Detaching tags
$articleItem->detachTags('third tag');
$articleItem->detachTags('third tag','some_type');
$articleItem->detachTags(['fourth tag', 'fifth tag']);
$articleItem->detachTags(['fourth tag', 'fifth tag'],'some_type');

// Get all tags of a model
$articleItem->tags;

// Syncing tags
$articleItem->syncTags(['first tag', 'second tag']); // all other tags on this model will be detached

// Retrieving models that have any of the given tags
ArticleItem::withAnyTags(['first tag', 'second tag'])->get();

// Retrieve models that have all of the given tags
ArticleItem::withAllTags(['first tag', 'second tag'])->get();

```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email mihakot@gmail.com instead of using the issue tracker.

## Credits

-   [Konstantin "MihaKot" Aksarin](https://gitlab.com/mihakot)


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Keywords

laravel, tags
