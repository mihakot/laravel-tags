# Changelog

All notable changes to `laravel-tags` will be documented in this file

## 0.1.7 - 27/12/2022

- fix error with dettach tags

## 0.1.3 - 12/07/2022

- update README.md
- 
## 0.1.0 - 11/07/2022

- initial release

