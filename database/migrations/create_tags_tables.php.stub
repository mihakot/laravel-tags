<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTables extends Migration
{
    public function up(): void
    {
        if (!Schema::hasTable('tags')) {
            Schema::create('tags', function (Blueprint $table) {
                $table->id('id');
                $table->string('name')->index();
                $table->timestampsTz();
            });
        }
        if (!Schema::hasTable('taggables')) {
            Schema::create('taggables', function (Blueprint $table) {
                $table->foreignId('tags_id')
                    ->constrained()
                    ->cascadeOnDelete();
                $table->morphs('taggable');
                $table->unique(['tags_id', 'taggable_id', 'taggable_type']);
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('taggables');
        Schema::dropIfExists('tags');
    }

}