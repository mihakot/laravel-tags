<?php

namespace Mihakot\Tags;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasTags
{
    /**
     * @var array
     */
    protected array $queuedTags = [];

    /**
     * Return Tags model name
     *
     * @return string
     */
    public static function getTagClassName(): string
    {
        return config('tags.tag_model', Tags::class);
    }

    /**
     * @return void
     */
    public static function bootHasTags(): void
    {
        static::created(function (Model $taggableModel) {
            /** @var self $taggableModel */
            if (count($taggableModel->queuedTags) === 0) {
                return;
            }
            $taggableModel->queuedTags = array_filter($taggableModel->queuedTags);

            $taggableModel->attachTags($taggableModel->queuedTags);

            $taggableModel->queuedTags = [];
        });

        static::deleted(function (Model $deletedModel) {
            /** @var self $deletedModel */
            $tags = $deletedModel->tags()->get()->toArray();
            if ($tags) {
                $deletedModel->detachTags($tags);
            }
        });
    }

    /**
     * Model tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags(): MorphToMany
    {
        return $this->morphToMany(self::getTagClassName(), 'taggable');
    }

    /**
     * @param $tags
     *
     * @return void
     */
    public function setTagsAttribute($tags): void
    {
        if (!$this->exists) {
            $this->queuedTags = $tags;
            return;
        }
        $this->syncTags($tags);
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array                                  $tags
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAllTags(Builder $query, array $tags): Builder
    {
        $tags = static::convertToTags($tags);

        collect($tags)->each(function ($tag) use ($query) {
            $query->whereHas('tags', function (Builder $query) use ($tag) {
                $query->where('tags.id', $tag->id ?? 0);
            });
        });

        return $query;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array                                  $tags
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithAnyTags(Builder $query, array $tags): Builder
    {
        $tags = static::convertToTags($tags);

        return $query->whereHas('tags', function (Builder $query) use ($tags) {
            $tagIds = collect($tags)->pluck('id');

            $query->whereIn('tags.id', $tagIds);
        });
    }

    /**
     * @param  array  $tags
     *
     * @return $this
     */
    public function attachTags(array $tags)
    {
        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags));

        $this->tags()->syncWithoutDetaching($tags->pluck('id')->toArray());

        return $this;
    }

    /**
     * @param $tag
     *
     * @return $this
     */
    public function attachTag($tag)
    {
        return $this->attachTags([$tag]);
    }

    /**
     * @param  array  $tags
     *
     * @return $this
     */
    public function detachTags(array $tags)
    {
        $tags = static::convertToTags($tags);

        collect($tags)->filter()->each(fn($tag) => $this->tags()->detach($tag));

        return $this;
    }

    /**
     * @param  string  $tag
     *
     * @return $this
     */
    public function detachTag(string $tag)
    {
        return $this->detachTags([$tag]);
    }

    /**
     * @param  array  $tags
     *
     * @return $this
     */
    public function syncTags(array $tags)
    {
        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags));

        $this->tags()->sync($tags->pluck('id')->toArray());

        return $this;
    }

    /**
     * @param $values
     *
     * @return \Illuminate\Support\Collection
     */
    protected static function convertToTags($values)
    {
        $className = static::getTagClassName();

        if ($values instanceof $className) {
            $values = [$values];
        }

        return collect($values)->map(function ($value) use ($className) {
            return $className::findFromString($value);
        });
    }

    /**
     * @param               $ids
     * @param  string|null  $type
     * @param  bool         $detaching
     *
     * @return void
     */
    protected function syncTagIds($ids, string $type = null, bool $detaching = true): void
    {
        $isUpdated = false;

        // Get a list of tag_ids for all current tags
        $current = $this->tags()
            ->newPivotStatement()
            ->where('taggable_id', $this->getKey())
            ->where('taggable_type', $this->getMorphClass())
            ->when($type !== null, function ($query) use ($type) {
                $tagModel = $this->tags()->getRelated();

                return $query
                    ->join($tagModel->getTable(), 'taggables.tag_id', '=',
                        $tagModel->getTable().'.'.$tagModel->getKeyName())
                    ->where($tagModel->getTable().'.type', $type);
            })->pluck('tag_id')->all();

        // Compare to the list of ids given to find the tags to remove
        $detach = array_diff($current, $ids);
        if ($detaching && count($detach) > 0) {
            $this->tags()->detach($detach);
            $isUpdated = true;
        }

        // Attach any new ids
        $attach = array_unique(array_diff($ids, $current));
        if (count($attach) > 0) {
            collect($attach)->each(function ($id) {
                $this->tags()->attach($id, []);
            });
            $isUpdated = true;
        }

        if ($isUpdated) {
            $this->tags()->touchIfTouching();
        }
    }
}