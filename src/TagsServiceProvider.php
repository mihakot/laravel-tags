<?php

namespace Mihakot\Tags;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class TagsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-tags')
            ->hasConfigFile()
            ->hasMigration('create_tags_tables');
    }
}