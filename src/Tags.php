<?php

namespace Mihakot\Tags;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Tags extends Model
{

    /** @var string[]  */
    protected $fillable = ['name'];

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string                                 $name
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeContaining(Builder $query, string $name): Builder
    {
        return $query->whereRaw('LOWER(name) LIKE ?', ['%'.mb_strtolower($name).'%']);
    }

    /**
     * @param $values
     *
     * @return Collection
     */
    public static function findOrCreate($values): Collection
    {
        $tags = collect($values)->map(function ($value) {
            if ($value instanceof self) {
                return $value;
            }
            return static::findOrCreateFromString($value);
        });
        return is_string($values) ? $tags->first() : $tags;
    }

    /**
     * @param  string  $name
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public static function findFromString(string $name): Model|null
    {
        return static::query()->where("name", $name)->first();
    }

    /**
     * @param  string  $name
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected static function findOrCreateFromString(string $name): Model|null
    {
        $tag = static::findFromString($name);
        if (!$tag) {
            $tag = static::create([
                'name' => $name,
            ]);
        }
        return $tag;
    }
}