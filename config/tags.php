<?php

return [
    /*
     * The fully qualified class name of the tag model.
     */
    'tag_model' => Mihakot\Tags\Tags::class,
];